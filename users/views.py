from rest_framework.response import Response
from rest_framework import status
from users.models import Admin
from users.serializers import UserSerializer
from rest_framework import viewsets
from AdminBackend.common import Users


################## LIST Y CREATE USER ######################

class UserList(viewsets.ModelViewSet):
    queryset = Admin.objects.all()
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, fields=('id', 'email', 'password', 'first_name', 'last_name',
                                                           'username', 'city', 'country', 'academic', 'address'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,
                                         fields=('id', 'email', 'password', 'first_name', 'last_name', 'username',
                                                 'city', 'country', 'academic', 'address'))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(
            {'message': "Successful registration...!"},
            status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()


################## DELETE AND UPDATE OF USER ######################
class UserDetail(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = None

    def retrieve(self, request, id):
        instance = Users.get_object(id)
        queryset = Admin.objects.get(id=instance.id)
        serializer = self.get_serializer(queryset, fields=('id', 'email', 'password', 'first_name', 'last_name',
                                                           'username', 'city', 'country', 'academic', 'address'),
                                         many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, **kwargs)

    def update(self, request, id, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = Users.get_object(id)
        serializer = self.get_serializer(instance, data=request.data,
                                         fields=('first_name', 'last_name', 'username', 'city', 'address' 'email'),
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def perform_update(self, serializer, request):
        serializer.save()

    def destroy(self, request, id):
        Users.get_object(id)
        Admin.objects.filter(id=id).update(is_active=0)
        return Response(status=status.HTTP_202_ACCEPTED)
