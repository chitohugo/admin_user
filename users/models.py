from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.


class Admin(AbstractUser):
    email = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=255, unique=True)
    first_name = models.CharField(max_length=50, unique=True)
    last_name = models.CharField(max_length=50, unique=True)
    username = models.CharField(max_length=50, unique=True)
    city = models.CharField(max_length=50, unique=True)
    country = models.CharField(max_length=50, unique=True)
    academic = models.CharField(max_length=50, unique=True)
    address = models.CharField(max_length=150, unique=True)

    class Meta:
        db_table = 'admin'

    def __unicode__(self):
        return unicode(self.id)

    def __str__(self):
        return 'Admin: {} {}'.format(self.id, self.username)
