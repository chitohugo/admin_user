from rest_framework import serializers
from users.models import Admin


class DynamicSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        super(DynamicSerializer, self).__init__(*args, **kwargs)
        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class UserSerializer(DynamicSerializer):
    class Meta:
        model = Admin
        fields = ('id', 'email', 'password', 'first_name', 'last_name', 'username', 'city', 'country', 'academic',
                  'address')

    def create(self, validated_data):
        return Admin.objects.create_user(**validated_data)

