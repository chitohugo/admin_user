from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url
from users import views

urlpatterns = [
    url(r'^users$', views.UserList.as_view({'get': 'list', 'post': 'create'}), name='user-list'),
    url(r'^users/(?P<id>[0-9]+)$', views.UserDetail.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
        name='user-detail')
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
