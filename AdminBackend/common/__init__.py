from django.http import Http404
from users.models import Admin


class Users(object):
    def get_object(id):
        try:
            return Admin.objects.get(id=id, is_active=True)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_active(id):
        try:
            return Admin.objects.get(id=id, is_active=True)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_username(username):
        try:
            return Admin.objects.get(username=username)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_email(email):
        try:
            return Admin.objects.get(email=email, is_active=True)
        except Admin.DoesNotExist:
            raise Http404
